//
//  AccueilViewController.h
//  TakeAPhoto
//
//  Created by Yahia  on 05/01/2016.
//  Copyright © 2016 Yahia . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ObjectiveFlickr/ObjectiveFlickr.h"
#import "CustomInfoWindow.h"
@import GoogleMaps;
@interface AccueilViewController : UIViewController<GMSMapViewDelegate,CLLocationManagerDelegate>{
    GMSMapView *mapView_;
    UIImagePickerController *picker;
    CustomInfoWindow *infoWindow;
    CLLocationCoordinate2D yAllerLocation ;
    UIButton *boutonAjoutPhoto;
    //UIButton *boutonPrendrePhoto;
    UIButton *boutonPrecis;
    UIButton *boutonMeLocaliser;
    UIImage *img;
    GMSCameraPosition *endroitSelectionne;
    GMSCameraPosition *maPosition;
    CLLocationManager* locationManager;
}
@property (nonatomic, copy) NSString *idflickr;
@property (nonatomic, assign) OFFlickrAPIRequest *flickrRequest;

@end
