//
//  AccueilViewController.m
//  TakeAPhoto
//
//  Created by Yahia  on 05/01/2016.
//  Copyright © 2016 Yahia . All rights reserved.
//

#import "AccueilViewController.h"
#import "ViewController.h"
#import "Constantes.h"
@interface AccueilViewController ()

@end

@implementation AccueilViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //On désactive l'onglet connexion
    [[[[self.tabBarController tabBar]items]objectAtIndex:0]setEnabled:FALSE];
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:TRUE];
    [[[[self.tabBarController tabBar]items]objectAtIndex:2]setEnabled:TRUE];
    [[[[self.tabBarController tabBar]items]objectAtIndex:3]setEnabled:TRUE];
    
    //Le bouton pour prendre des photos par rapport à une demande
    /*boutonPrendrePhoto = [UIButton buttonWithType:UIButtonTypeCustom];
    [boutonPrendrePhoto addTarget:self
                         action:@selector(PrendrePhoto:)
               forControlEvents:UIControlEventTouchUpInside];
    [boutonPrendrePhoto setBackgroundImage:[UIImage imageNamed:@"photo-camera.png"] forState:UIControlStateNormal];
    boutonPrendrePhoto.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width -50, 80 , 32.0, 32.0);
    [self.view addSubview:boutonPrendrePhoto];
    boutonPrendrePhoto.hidden = YES;*/
    
    //Le bouton pour ajouter des photos par rapport à une demande
    boutonAjoutPhoto = [UIButton buttonWithType:UIButtonTypeCustom];
    [boutonAjoutPhoto addTarget:self
                         action:@selector(ouvrirGaleriePhoto:)
               forControlEvents:UIControlEventTouchUpInside];
    [boutonAjoutPhoto setBackgroundImage:[UIImage imageNamed:@"pictures.png"] forState:UIControlStateNormal];
    boutonAjoutPhoto.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width -50, 115 , 32.0, 32.0);
    [self.view addSubview:boutonAjoutPhoto];
    boutonAjoutPhoto.hidden = YES;
    
    //Bouton zoom si l'endroit selectionne n'est pas assez précis
    boutonPrecis = [UIButton buttonWithType:UIButtonTypeCustom];
    [boutonPrecis addTarget:self
                         action:@selector(PlusDePrecision:)
               forControlEvents:UIControlEventTouchUpInside];
    [boutonPrecis setBackgroundImage:[UIImage imageNamed:@"zoom-out.png"] forState:UIControlStateNormal];
    boutonPrecis.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width -50, 155 , 32.0, 32.0);
    [self.view addSubview:boutonPrecis];
    boutonPrecis.hidden = YES;
    
    //La toolbar du bas qui permet de se rendre au point selectionne
    UIToolbar* toolbar = [[UIToolbar alloc] init];
    toolbar.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height - 90, [[UIScreen mainScreen] bounds].size.width, 44);
    UIBarButtonItem *flexibleBar = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *BoutonYAller = [[UIBarButtonItem alloc] initWithTitle:@"Y Aller" style:UIBarButtonItemStyleBordered target:self action:@selector(allerAuPoint:)];
    NSArray *buttonItems = [NSArray arrayWithObjects:flexibleBar,BoutonYAller,flexibleBar,nil];
    [toolbar setItems:buttonItems];
    [self.view addSubview:toolbar];
    
    
    //Fond noir ou il y a les logos
    UIImageView *fondNavBar = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 65)];
    [fondNavBar setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:fondNavBar];
    
    //petit logo a gauche de Take A photo
    UIImageView *logoTAP = [[UIImageView alloc] initWithFrame:CGRectMake(12, 33, 30, 22)];
    logoTAP.image = [UIImage imageNamed:@"logo_without_title.png"];
    logoTAP.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:logoTAP];
    
    //label TAP
    UILabel *labelTAP = [[UILabel alloc]initWithFrame:CGRectMake(100, 33, 119, 21)];
    labelTAP.textAlignment = NSTextAlignmentRight;
    labelTAP.textColor = [UIColor whiteColor];
    [labelTAP setFont:[UIFont fontWithName:@"Arial" size:15.0]];
    labelTAP.text = @"TAKE A PHOTO";
    [self.view addSubview:labelTAP];
    
    
    //Ajout de la geolocalisation
    locationManager = [[CLLocationManager alloc] init];
    if ([CLLocationManager locationServicesEnabled])
    {
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter = 500.0f;
        [locationManager requestWhenInUseAuthorization];
        [locationManager startUpdatingLocation];
    }
    
    //Bouton zoom si l'endroit selectionne n'est pas assez précis
    boutonMeLocaliser = [UIButton buttonWithType:UIButtonTypeCustom];
    [boutonMeLocaliser addTarget:self
                     action:@selector(OuSuisJe:)
           forControlEvents:UIControlEventTouchUpInside];
    [boutonMeLocaliser setBackgroundImage:[UIImage imageNamed:@"gps.png"] forState:UIControlStateNormal];
    boutonMeLocaliser.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width -65, [[UIScreen mainScreen] bounds].size.height - 170 , 64.0, 64.0);
    [self.view addSubview:boutonMeLocaliser];
    
    
    NSLog(@"voila le flickr id %@",_idflickr);
}

-(void)viewWillAppear:(BOOL)animated{
    [mapView_ clear];
    //Requete pour recuperer l'ensemble des points sur la map TAP
    //NSURL *urlreq = [NSURL URLWithString:@"http://jules-vanneste.fr/takeaphotoforme/get_demandes.php"];
    NSURL *urlreq = [NSURL URLWithString:GET_DEMANDES];
    NSData *data = [NSData dataWithContentsOfURL:urlreq];
    NSString *ret = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"ret=%@", ret);
    NSMutableArray *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    NSLog(@"Result = %@",result);
    NSArray *Array = [result valueForKey:@"demandes"];
    
    for (NSMutableDictionary *dic in Array)
    {
        NSString *latitude = [dic valueForKey:@"latitude"];
        NSString *longitude = [dic valueForKey:@"longitude"];
        NSString *description = [dic valueForKey:@"description"];
        NSString *id_demande = [dic valueForKey:@"id_demande"];
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake([latitude doubleValue],  [longitude doubleValue]);
        marker.title = description;
        marker.snippet = id_demande;
        marker.map = mapView_;
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//Permet le chargement de la google map avec le zoom defini
- (void)loadView {
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:50.611976
                                                            longitude:3.142528
                                                                 zoom:12];
    mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView_.delegate = self;
    /*mapView_.myLocationEnabled = YES;
    NSLog(@"User's location: %@", mapView_.myLocation);
    mapView_.settings.myLocationButton = YES;*/
    
    self.view = mapView_;
}

//Si il y a un long press on propose d'ajouter une demande si le zoom est suffisant
-(void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate{
    if(mapView.camera.zoom> 15){
        NSLog(@"You tapped at %f,%f", coordinate.latitude, coordinate.longitude);
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Ma demande"
                                      message:nil
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       UITextField *valeurTitre;
                                                       //Do Some action here
                                                       // Creates a marker in the center of the map.
                                                       NSString *text = ((UITextField *)[alert.textFields objectAtIndex:0]).text;
                                                       GMSMarker *marker = [[GMSMarker alloc] init];
                                                       marker.position = CLLocationCoordinate2DMake(coordinate.latitude,  coordinate.longitude);
                                                       marker.title = text;
                                                       //marker.snippet = @"Snippet";
                                                       marker.map = mapView_;
                                                       //NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://jules-vanneste.fr/takeaphotoforme/ajout_demande.php"]];
                                                       NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:AJOUT_DEMANDE]];
                                                       [request setValue:@"gzip" forHTTPHeaderField:@"Accept-Encoding"];
                                                       [request setHTTPBody:[[NSString stringWithFormat:@"latitude=%f&longitude=%f&description=%@&idUser=%@",coordinate.latitude,coordinate.longitude,text,_idflickr] dataUsingEncoding:NSUTF8StringEncoding]];
                                                       [request setHTTPMethod:@"POST"];
                                                       NSError *error = nil; NSURLResponse *response = nil;
                                                       NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                                                       if (error) {
                                                           NSLog(@"Error:%@", error.localizedDescription);
                                                       }
                                                       else {
                                                           //success
                                                       }
                                                       NSLog(@"bouton ok %@", text);
                                                       
                                                   }];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"Titre";
            NSLog(@"voici le titre %@" , textField.text);
        }];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        endroitSelectionne = [GMSCameraPosition cameraWithLatitude:coordinate.latitude
                                                                longitude:coordinate.longitude
                                                                     zoom:16];
        boutonPrecis.hidden = NO;
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Il faut être plus précis sur la map pour l'ajout d'une demande"
                                      message:nil
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

//on cache l'ensemble des boutons
- (void)mapView:(GMSMapView *)mapView
didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    boutonAjoutPhoto.hidden = YES;
    boutonPrecis.hidden = YES;
    //boutonPrendrePhoto.hidden = YES;
}

-(void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
}

//On sauvegarde le point sur lequel l'utilisateur clique pour par exemple pour l'y emmener si il souhaite y aller
-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{
    ViewController *ongletConnexion = (ViewController *)[[self.tabBarController viewControllers]objectAtIndex:0];
    ongletConnexion.id_demande = marker.snippet;
    marker.icon = [GMSMarker markerImageWithColor:[UIColor blackColor]];
    yAllerLocation =  marker.position;
    boutonAjoutPhoto.hidden = NO;
    //boutonPrendrePhoto.hidden = NO;
    return false;
}

//Apres clique sur le bouton la galerie photo est ouverte
- (IBAction)ouvrirGaleriePhoto:(id)sender {
    picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentModalViewController:picker animated:YES];
}

//Permet d'afficher la bulle custom qui contient les infos du marker
-(UIView *) mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker{
    infoWindow = [[[NSBundle mainBundle] loadNibNamed:@"InfoWindow" owner:self options:nil] objectAtIndex:0];
    infoWindow.titre.text = marker.title;
    if (img) {
        infoWindow.imageInfoWindow.image = img;
    }
    return infoWindow;
}

//Methode qui est declenchee apres la selection d'une photo dans la galerie
- (void)imagePickerController:(UIImagePickerController *) Picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    img = (UIImage *)[info objectForKey:UIImagePickerControllerOriginalImage];
    NSData *photo = UIImageJPEGRepresentation(img, 1.0);
    NSInputStream *imageStream = [NSInputStream inputStreamWithData:photo];
    [_flickrRequest uploadImageStream:imageStream suggestedFilename:@"test.jpg" MIMEType:@"image/jpeg" arguments:[NSDictionary dictionaryWithObjectsAndKeys:@"1",@"is_public",nil]];
    [picker dismissModalViewControllerAnimated:YES];
}

//Lance le parcours gps pour se rendre au point selectionner
- (IBAction)allerAuPoint:(id)sender {
    
    MKPlacemark* place = [[MKPlacemark alloc] initWithCoordinate:yAllerLocation addressDictionary: nil];
    MKMapItem* destination = [[MKMapItem alloc] initWithPlacemark: place];
    destination.name = infoWindow.titre.text;
    NSArray* items = [[NSArray alloc] initWithObjects: destination, nil];
    NSDictionary* options = [[NSDictionary alloc] initWithObjectsAndKeys:
                             MKLaunchOptionsDirectionsModeDriving,
                             MKLaunchOptionsDirectionsModeKey, nil];
    [MKMapItem openMapsWithItems: items launchOptions: options];
}

//Permet a l'utilisateur de zoom sur l'endroit qu'il a selectionne pour l'ajout d'un point
- (IBAction)PlusDePrecision:(id)sender {
    [mapView_ setCamera:endroitSelectionne];
    boutonPrecis.hidden = YES;
}

//Tout les 500 mètres on récupère la nouvelle localisation
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    //NSLog(@"%@ nouvelle localisation",newLocation);
    maPosition = [GMSCameraPosition cameraWithLatitude:newLocation.coordinate.latitude
                                                     longitude:newLocation.coordinate.longitude
                                                          zoom:13];
}


- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    
}

- (IBAction)OuSuisJe:(id)sender {
    [mapView_ setCamera:maPosition];
}

/*
- (IBAction)PrendrePhoto:(id)sender {
    picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentModalViewController:picker animated:YES];
    boutonPrendrePhoto.hidden = YES;
}*/
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
