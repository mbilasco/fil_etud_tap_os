//
//  Constantes.h
//  TakeAPhoto
//
//  Created by Yahia  on 20/02/2016.
//  Copyright © 2016 Yahia . All rights reserved.
//

#ifndef Constantes_h
#define Constantes_h

static NSString *GET_DEMANDES =  @"http://localhost:8888/fil_etud_tap_server/get_demandes.php";
static NSString *AJOUT_DEMANDE = @"http://localhost:8888/fil_etud_tap_server/ajout_demande.php";
static NSString *MES_DEMANDES = @"http://localhost:8888/fil_etud_tap_server/my_demandes.php";
static NSString *GET_URL_PHOTO = @"http://localhost:8888/fil_etud_tap_server/get_url_photo_demande.php";
#endif /* Constantes_h */
