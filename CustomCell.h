//
//  CustomCell.h
//  TakeAPhoto
//
//  Created by Yahia  on 15/02/2016.
//  Copyright © 2016 Yahia . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titre;

@end
