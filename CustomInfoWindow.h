//
//  CustomInfoWindow.h
//  TakeAPhoto
//
//  Created by Yahia  on 14/02/2016.
//  Copyright © 2016 Yahia . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomInfoWindow : UIView

@property (weak, nonatomic) IBOutlet UILabel *titre;
@property (weak, nonatomic) IBOutlet UIImageView *imageInfoWindow;
@end
