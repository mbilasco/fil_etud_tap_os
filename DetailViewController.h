//
//  DetailViewController.h
//  TakeAPhoto
//
//  Created by Yahia  on 16/02/2016.
//  Copyright © 2016 Yahia . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController{
    NSString *ChaineIdDemande;
    NSString *ChaineIdUser;
    NSMutableArray *TaburlPhotos;
    UIImageView *imgReponse;
    UIButton *validerBoutton;
}
@property (weak, nonatomic) IBOutlet UILabel *donneeRecu;
@property (strong, nonatomic) id texteAAfficher;
@property (strong, nonatomic) id id_demande;
@property (strong, nonatomic) id id_user;
@end
