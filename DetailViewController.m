//
//  DetailViewController.m
//  TakeAPhoto
//
//  Created by Yahia  on 16/02/2016.
//  Copyright © 2016 Yahia . All rights reserved.
//

#import "DetailViewController.h"
#import "Constantes.h"
@interface DetailViewController ()

@end

@implementation DetailViewController

@synthesize texteAAfficher = _texteAAfficher;
@synthesize  id_demande = _id_demande;
static int i = 0;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Recuperation de l'id utilisateur
    ChaineIdUser = _id_user;
    
    //Modification de la couleur de la navigation bar
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    //Image de fond
    //Fond noir ou il y a les logos
    UIImageView *imgFond = [[UIImageView alloc] initWithFrame:CGRectMake(0, 64,[[UIScreen mainScreen] bounds].size.width ,260 )];
    imgFond.image = [UIImage imageNamed:@"fondnoir.jpeg"];
    imgFond.contentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:imgFond];
    
    //Image affichee
    imgReponse = [[UIImageView alloc] initWithFrame:CGRectMake(0, 135,[[UIScreen mainScreen] bounds].size.width ,[[UIScreen mainScreen] bounds].size.height - 134 )];
    [self.view addSubview:imgReponse];
    
    //Affichage du titre de photo recu
    _donneeRecu.text = _texteAAfficher;
    UILabel *titrePhoto = [[UILabel alloc]initWithFrame:CGRectMake(0, 148, [[UIScreen mainScreen] bounds].size.width, 70)];
    titrePhoto.textAlignment = NSTextAlignmentRight;
    titrePhoto.textColor = [UIColor whiteColor];
    [titrePhoto setFont:[UIFont fontWithName:@"Arial" size:18.0]];
    titrePhoto.text = _donneeRecu.text;
    titrePhoto.textAlignment = NSTextAlignmentCenter;
    titrePhoto.numberOfLines = 0;
    [self.view addSubview:titrePhoto];
    
    //on recupere l'id de la demande
    ChaineIdDemande = _id_demande;
    TaburlPhotos = [NSMutableArray array];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:GET_URL_PHOTO]];
    [request setValue:@"gzip" forHTTPHeaderField:@"Accept-Encoding"];
    [request setHTTPBody:[[NSString stringWithFormat:@"id_demande=%@",_id_demande] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    NSError *error = nil; NSURLResponse *response = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    NSMutableArray *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSArray *Array = [result valueForKey:@"reponses"];
    if (error) {
        NSLog(@"Error:%@", error.localizedDescription);
    }
    else {
        
        for (NSMutableDictionary *dic in Array)
        {
            
            NSString *urlPhoto = [dic valueForKey:@"url"];
            [TaburlPhotos addObject:urlPhoto];
            //NSLog(@"Voici la latitude %@", latitude);
        }
    }
    
    if([TaburlPhotos count] !=0){
        
        //Toolbar du bas
        UIToolbar* toolbarBas = [[UIToolbar alloc] init];
        toolbarBas.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height - 110, [[UIScreen mainScreen] bounds].size.width, 64);
        toolbarBas.translucent = NO;
        toolbarBas.barTintColor = [UIColor blackColor];
        
        //Les boutons de la toolbar
        
        //L'espace entre les deux boutons
        UIBarButtonItem *flexibleBar = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        
        //Bouton suivant
        UIButton *btnSuivant = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnSuivant setFrame:CGRectMake(0, 0, 64, 64)];
        [btnSuivant setBackgroundImage:[UIImage imageNamed:@"right-arrow64.png"] forState:UIControlStateNormal];
        [btnSuivant addTarget:self action:@selector(photoSuivante:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *suivantItem = [[UIBarButtonItem alloc] initWithCustomView:btnSuivant];
        
        //Bouton precedent
        UIButton *btnPrecedent = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnPrecedent setFrame:CGRectMake(0, 0, 64, 64)];
        [btnPrecedent setBackgroundImage:[UIImage imageNamed:@"left-arrow64.png"] forState:UIControlStateNormal];
        [btnPrecedent addTarget:self action:@selector(photoPrecedente:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *precedentItem = [[UIBarButtonItem alloc] initWithCustomView:btnPrecedent];
        
        NSArray *buttonItems = [NSArray arrayWithObjects: precedentItem,flexibleBar,suivantItem,nil];
        [toolbarBas setItems:buttonItems];
        [self.view addSubview:toolbarBas];
        
        
        //Toolbar du haut
        UIToolbar* toolbarHaut = [[UIToolbar alloc] init];
        toolbarHaut.frame = CGRectMake(0, 65, [[UIScreen mainScreen] bounds].size.width, 44);
        toolbarHaut.translucent = NO;
        toolbarHaut.barTintColor = [UIColor blackColor];
        
        //download item
        UIButton *downloadBoutton = [self BoutonACharger:@"download.png"];;
        [downloadBoutton addTarget:self action:@selector(TelechargerPhoto:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *downloadItem = [[UIBarButtonItem alloc] initWithCustomView:downloadBoutton];
        
        //valider Item
        validerBoutton = [self BoutonACharger:@"checked-buttom.png"];;
        [validerBoutton addTarget:self action:@selector(ValiderPoint:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *validerItem = [[UIBarButtonItem alloc] initWithCustomView:validerBoutton];
        
        NSArray *buttonItemsHaut = [NSArray arrayWithObjects:downloadItem,flexibleBar,validerItem, nil];
        [toolbarHaut setItems:buttonItemsHaut];
        [self.view addSubview:toolbarHaut];
        
        NSURL *uneImage = [NSURL URLWithString:[TaburlPhotos objectAtIndex:0]];
        imgReponse.image = [UIImage imageWithData: [NSData dataWithContentsOfURL: uneImage]];
        imgReponse.contentMode = UIViewContentModeScaleAspectFit;
    }
    else{
        titrePhoto.text = @"Aucune Photo Disponible";
        imgReponse.image = [UIImage imageNamed:@"patience.jpg"];
        imgReponse.contentMode = UIViewContentModeScaleAspectFit;
    }
}


- (IBAction)photoSuivante:(id)sender {
    if(i < [TaburlPhotos count] -1){
        i++;
        NSURL *uneImage = [NSURL URLWithString:[TaburlPhotos objectAtIndex:i]];
        imgReponse.image = [UIImage imageWithData: [NSData dataWithContentsOfURL: uneImage]];
        
    }
    else{
        i=0;
        NSURL *uneImage = [NSURL URLWithString:[TaburlPhotos objectAtIndex:i]];
        imgReponse.image = [UIImage imageWithData: [NSData dataWithContentsOfURL: uneImage]];
        
    }
}

- (IBAction)photoPrecedente:(id)sender {
    if(i > 0){
        i--;
        NSURL *uneImage = [NSURL URLWithString:[TaburlPhotos objectAtIndex:i]];
        imgReponse.image = [UIImage imageWithData: [NSData dataWithContentsOfURL: uneImage]];
    }
    else{
        i= [TaburlPhotos count] -1;
        NSURL *uneImage = [NSURL URLWithString:[TaburlPhotos objectAtIndex:i]];
        imgReponse.image = [UIImage imageWithData: [NSData dataWithContentsOfURL: uneImage]];
        
    }
}

//Methode lorsque l'utilisateur souhaite enregistrer la photo
- (IBAction)TelechargerPhoto:(id)sender {
    UIImageWriteToSavedPhotosAlbum(imgReponse.image, nil, nil, nil);
}

//On supprime la demande en base
- (IBAction)ValiderPoint:(id)sender {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://localhost:8888/fil_etud_tap_server/del_demande.php"]];
    [request setValue:@"gzip" forHTTPHeaderField:@"Accept-Encoding"];
    [request setHTTPBody:[[NSString stringWithFormat:@"id_user=%@&id_demande=%@",ChaineIdUser, _id_demande] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    NSError *error = nil; NSURLResponse *response = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    NSMutableArray *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    if (error) {
        NSLog(@"Error:%@", error.localizedDescription);
    }
    else {
        [validerBoutton setBackgroundImage:[UIImage imageNamed:@"checked-buttom-vert.png"] forState:UIControlStateNormal];
    }
}

-(UIButton *)BoutonACharger:(NSString *)NomImg{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 24, 24)];
    [btn setBackgroundImage:[UIImage imageNamed:NomImg] forState:UIControlStateNormal];
    
    return btn;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
