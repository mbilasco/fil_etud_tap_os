//
//  MesDemandesMapViewController.h
//  TakeAPhoto
//
//  Created by Yahia  on 17/02/2016.
//  Copyright © 2016 Yahia . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomInfoWindow.h"
@import GoogleMaps;
@interface MesDemandesMapViewController : UIViewController<GMSMapViewDelegate>{
    GMSMapView *mapView_;
    CustomInfoWindow *infoWindow;
    UIButton *boutonSuppr;
    GMSMarker *markerSelectionne;
}
@property (nonatomic, copy) NSString *idflickr;


@end
