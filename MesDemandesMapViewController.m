//
//  MesDemandesMapViewController.m
//  TakeAPhoto
//
//  Created by Yahia  on 17/02/2016.
//  Copyright © 2016 Yahia . All rights reserved.
//

#import "MesDemandesMapViewController.h"
#import "Constantes.h"
@interface MesDemandesMapViewController ()

@end

@implementation MesDemandesMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Fond noir
    UIImageView *fondNavBar = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 65)];
    [fondNavBar setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:fondNavBar];
    
    //petit logo a gauche de Take A photo
    UIImageView *logoTAP = [[UIImageView alloc] initWithFrame:CGRectMake(12, 33, 30, 22)];
    logoTAP.image = [UIImage imageNamed:@"logo_without_title.png"];
    logoTAP.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:logoTAP];
    
    //label TAP
    UILabel *labelTAP = [[UILabel alloc]initWithFrame:CGRectMake(100, 33, 119, 21)];
    labelTAP.textAlignment = NSTextAlignmentRight;
    labelTAP.textColor = [UIColor whiteColor];
    [labelTAP setFont:[UIFont fontWithName:@"Arial" size:15.0]];
    labelTAP.text = @"TAKE A PHOTO";
    [self.view addSubview:labelTAP];
    
    //Bouton pour supprimer une demande
    boutonSuppr = [UIButton buttonWithType:UIButtonTypeCustom];
    [boutonSuppr addTarget:self
                     action:@selector(SupprimerPoint:)
           forControlEvents:UIControlEventTouchUpInside];
    [boutonSuppr setBackgroundImage:[UIImage imageNamed:@"cancel-button.png"] forState:UIControlStateNormal];
    boutonSuppr.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width -50, 120 , 32.0, 32.0);
    [self.view addSubview:boutonSuppr];
    boutonSuppr.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//On récupère l'ensemble des points de l'utilisateur
-(void) viewWillAppear:(BOOL)animated{
    [mapView_ clear];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:MES_DEMANDES]];
    [request setValue:@"gzip" forHTTPHeaderField:@"Accept-Encoding"];
    [request setHTTPBody:[[NSString stringWithFormat:@"idUser=%@",_idflickr] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    NSError *error = nil; NSURLResponse *response = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    NSMutableArray *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSArray *Array = [result valueForKey:@"demande"];
    if (error) {
        NSLog(@"Error:%@", error.localizedDescription);
    }
    else {
        
        for (NSMutableDictionary *dic in Array)
        {
            NSString *latitude = [dic valueForKey:@"latitude"];
            NSString *longitude = [dic valueForKey:@"longitude"];
            NSString *description = [dic valueForKey:@"description"];
            NSString *id_demande = [dic valueForKey:@"id_demande"];
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.position = CLLocationCoordinate2DMake([latitude doubleValue],  [longitude doubleValue]);
            marker.title = description;
            marker.snippet = id_demande;
            marker.map = mapView_;
            
        }
    }
    
}
//Chargement de la map
- (void)loadView {
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:50.611976
                                                            longitude:3.142528
                                                                 zoom:12];
    mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView_.delegate = self;
    mapView_.settings.myLocationButton = YES;
    
    self.view = mapView_;
}

//Chargement de la CustomInfoWindow
-(UIView *) mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker{
    infoWindow = [[[NSBundle mainBundle] loadNibNamed:@"InfoWindow" owner:self options:nil] objectAtIndex:0];
    infoWindow.titre.text = marker.title;
    infoWindow.imageInfoWindow.image = [UIImage imageNamed:@"lille.jpg"];
    return infoWindow;
}

//On sauvegarde le point sur lequel l'utilisateur clique pour le supprimer s'il le souhaite
-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{
    markerSelectionne = marker;
    boutonSuppr.hidden = NO;
    return false;
}

- (void)mapView:(GMSMapView *)mapView
didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    boutonSuppr.hidden = YES;
}



//Requete pour supprimer un point
- (IBAction)SupprimerPoint:(id)sender {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://localhost:8888/fil_etud_tap_server/del_demande.php"]];
    [request setValue:@"gzip" forHTTPHeaderField:@"Accept-Encoding"];
    [request setHTTPBody:[[NSString stringWithFormat:@"id_user=%@&id_demande=%@",_idflickr, markerSelectionne.snippet ] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    NSError *error = nil; NSURLResponse *response = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    NSMutableArray *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    if (error) {
        NSLog(@"Error:%@", error.localizedDescription);
    }
    else {
        boutonSuppr.hidden = YES;
        markerSelectionne.map = nil;
    }

}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
