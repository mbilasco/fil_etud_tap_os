//
//  MesDemandesTableViewController.m
//  TakeAPhoto
//
//  Created by Yahia  on 15/02/2016.
//  Copyright © 2016 Yahia . All rights reserved.
//

#import "MesDemandesTableViewController.h"
#import "DetailViewController.h"
#import "Constantes.h"
@interface MesDemandesTableViewController ()

@end

@implementation MesDemandesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Modification de la couleur de la navigation bar
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    //self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    maListe = [NSMutableArray array];
    DemandesId = [NSMutableArray array];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:MES_DEMANDES]];
    [request setValue:@"gzip" forHTTPHeaderField:@"Accept-Encoding"];
    [request setHTTPBody:[[NSString stringWithFormat:@"idUser=%@",_idflickr] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    NSError *error = nil; NSURLResponse *response = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    NSMutableArray *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSArray *Array = [result valueForKey:@"demande"];
    if (error) {
        NSLog(@"Error:%@", error.localizedDescription);
    }
    else {
        
        for (NSMutableDictionary *dic in Array)
        {
            
            NSString *description = [dic valueForKey:@"description"];
            NSString *id_demande = [dic valueForKey:@"id_demande"];
            [maListe addObject:description];
            [DemandesId addObject:id_demande];
            //NSLog(@"Voici la latitude %@", latitude);
        }
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [maListe count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Retrieve cell
    NSString *cellIdentifier = @"BasicCell";
    
    /* test */
    myCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(!myCell)
    {
        myCell = [[CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    myCell.titre.text = [maListe objectAtIndex:indexPath.row];
    myCell.titre.textColor = [UIColor whiteColor];
    [myCell setBackgroundColor:[UIColor grayColor]];
    return myCell;

}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"detailSegue"])
    {
        NSInteger selectedIndex = [[self.tableView indexPathForSelectedRow] row];
        DetailViewController *dvc = [segue destinationViewController];
        dvc.texteAAfficher = [NSString stringWithFormat:@"%@", [maListe objectAtIndex:selectedIndex]];
        dvc.id_demande = [NSString stringWithFormat:@"%@", [DemandesId objectAtIndex:selectedIndex]];
        dvc.id_user = _idflickr;
    }
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
