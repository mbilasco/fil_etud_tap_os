//
//  AppDelegate.h
//  TakeAPhoto
//
//  Created by Yahia  on 19/11/2015.
//  Copyright © 2015 Yahia . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjectiveFlickr/ObjectiveFlickr.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, assign) OFFlickrAPIRequest *flickrRequest;
@end

