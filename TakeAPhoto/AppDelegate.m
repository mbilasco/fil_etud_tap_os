//
//  AppDelegate.m
//  TakeAPhoto
//
//  Created by Yahia  on 19/11/2015.
//  Copyright © 2015 Yahia . All rights reserved.
//

#import "AppDelegate.h"
#import "APIkey.h"
#import "ViewController.h"
@import GoogleMaps;
@interface AppDelegate ()

@end

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


@implementation AppDelegate

-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
    
    
    NSString *_requestToken= nil;
    NSString *_verifier = nil;
    
    BOOL result = OFExtractOAuthCallback(url, [NSURL URLWithString:kCallbackURLBaseString], &_requestToken, &_verifier);
    if (!result) {
        NSLog(@"Invalid callback URL");
    }
    [_flickrRequest fetchOAuthAccessTokenWithRequestToken:_requestToken verifier:_verifier];
    return YES;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [GMSServices provideAPIKey:GOOGLE_MAPS_KEY];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        
        //[[UINavigationBar appearance] setBarTintColor:[UIColor blackColor] ];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        
    }

    return YES;
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
