//
//  ViewController.h
//  TakeAPhoto
//
//  Created by Yahia  on 19/11/2015.
//  Copyright © 2015 Yahia . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjectiveFlickr/ObjectiveFlickr.h"
#import "APIkey.h"
#import "AppDelegate.h"
#import <Foundation/Foundation.h>
static NSString *kOAuthAuth = @"OAuth";
static NSString *kCallbackURLBaseString = @"oatransdemo://callback";
static NSString *kFrobRequest = @"Frob";

@interface ViewController : UIViewController<OFFlickrAPIRequestDelegate>{
    OFFlickrAPIContext *_flickrContext;
    OFFlickrAPIRequest *_flickrRequest;
    NSString *user_id;
}

@property (nonatomic, copy) NSString *id_demande;

- (IBAction)connexionYahoo:(id)sender;


@end

