//
//  ViewController.m
//  TakeAPhoto
//
//  Created by Yahia  on 19/11/2015.
//  Copyright © 2015 Yahia . All rights reserved.
//
//
//  ViewController.m
//  flickrAuth
//
//  Created by Yahia  on 17/10/2015.
//  Copyright © 2015 Yahia . All rights reserved.
//

#import "ViewController.h"
#import "AccueilViewController.h"
#import "MesDemandesTableViewController.h"
#import "MesDemandesMapViewController.h"
@interface ViewController ()

@end
@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:FALSE];
    [[[[self.tabBarController tabBar]items]objectAtIndex:2]setEnabled:FALSE];
    [[[[self.tabBarController tabBar]items]objectAtIndex:3]setEnabled:FALSE];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest didCompleteWithResponse:(NSDictionary *)inResponseDictionary{
    //Envoi de l'url de la photo au serveur
    if([inResponseDictionary objectForKey:@"photo"]){
        // * http://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg
        NSLog(@"Recupération url photo");
        NSDictionary *dict=[inResponseDictionary valueForKeyPath:@"photo"];
        NSString *photoID = [dict objectForKey:@"id"];
        NSString *secret = [dict objectForKey:@"secret"];
        NSString *server = [dict objectForKey:@"server"];
        NSString *farmID = [dict objectForKey:@"farm"];
        NSString *url = [NSString stringWithFormat:@"http://farm%@.staticflickr.com/%@/%@_%@.jpg", farmID,server,photoID,secret];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://localhost:8888/fil_etud_tap_server/ajout_reponse.php"]];
        [request setValue:@"gzip" forHTTPHeaderField:@"Accept-Encoding"];
        [request setHTTPBody:[[NSString stringWithFormat:@"url=%@&idDemande=%@",url,_id_demande] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPMethod:@"POST"];
        NSError *error = nil; NSURLResponse *response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if (error) {
            NSLog(@"Error:%@", error.localizedDescription);
        }
        else {
        }
        
    }
    else{
        NSString *id_photo=inResponseDictionary[@"photoid"][@"_text"];
        [_flickrRequest callAPIMethodWithPOST:@"flickr.photos.getInfo" arguments:[NSDictionary dictionaryWithObjectsAndKeys:OBJECTIVE_FLICKR_SAMPLE_API_KEY, @"api_key", id_photo, @"photo_id", nil]];
    }
    
    
}


- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest didObtainOAuthRequestToken:(NSString *)inRequestToken secret:(NSString *)inSecret;
{
    
    _flickrContext.OAuthToken = inRequestToken;
    _flickrContext.OAuthTokenSecret = inSecret;
    
    NSURL *authURL = [_flickrContext userAuthorizationURLWithRequestToken:inRequestToken requestedPermission:OFFlickrWritePermission];
    NSLog(@"Auth URL: %@", [authURL absoluteString]);
    //[[NSWorkspace sharedWorkspace] openURL:authURL];
    [[UIApplication sharedApplication] openURL:authURL];
    
    //Passage du request au delegate
    AppDelegate *myAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    myAppDelegate.flickrRequest = _flickrRequest;
}

- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest didObtainOAuthAccessToken:(NSString *)inAccessToken secret:(NSString *)inSecret userFullName:(NSString *)inFullName userName:(NSString *)inUserName userNSID:(NSString *)inNSID
{
    _flickrContext.OAuthToken = inAccessToken;
    _flickrContext.OAuthTokenSecret = inSecret;
    
    NSLog(@"Token: %@, secret: %@", inAccessToken, inSecret);
    
    AccueilViewController *accueil = (AccueilViewController *)[[self.tabBarController viewControllers]objectAtIndex:1];
    MesDemandesMapViewController *mesDemandesMap = (MesDemandesMapViewController *)[[self.tabBarController viewControllers]objectAtIndex:2];
    UINavigationController *nav = [self.tabBarController.viewControllers objectAtIndex:3];
    MesDemandesTableViewController *mesDemandes = (MesDemandesTableViewController*) [nav.viewControllers objectAtIndex:0];

    NSRange range = [inNSID rangeOfString:@"%"];
    NSRange range2 = NSMakeRange(range.location,3);
    user_id = [inNSID stringByReplacingCharactersInRange:range2 withString:@"@"];
    accueil.idflickr = user_id;
    mesDemandesMap.idflickr = user_id;
    accueil.flickrRequest = _flickrRequest;
    mesDemandes.idflickr = user_id;
    [self.tabBarController setSelectedIndex:1];
}

- (IBAction)connexionYahoo:(id)sender {
    _flickrContext = [[OFFlickrAPIContext alloc] initWithAPIKey:OBJECTIVE_FLICKR_SAMPLE_API_KEY sharedSecret:OBJECTIVE_FLICKR_SAMPLE_API_SHARED_SECRET];
    
    _flickrRequest = [[OFFlickrAPIRequest alloc] initWithAPIContext:_flickrContext];
    _flickrRequest.delegate = self;
    _flickrRequest.requestTimeoutInterval = 60.0;
    
    
    _flickrRequest.sessionInfo = kOAuthAuth;
    [_flickrRequest fetchOAuthRequestTokenWithCallbackURL:[NSURL URLWithString:kCallbackURLBaseString]];
    
    //[_oldStyleAuthButton setEnabled:NO];
    //[_oauthAuthButton setEnabled:NO];
}
@end
