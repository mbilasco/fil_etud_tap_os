//
//  main.m
//  TakeAPhoto
//
//  Created by Yahia  on 19/11/2015.
//  Copyright © 2015 Yahia . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
