//
//  projectColors.h
//  Resistance
//

#include <UIKit/UIKit.h>
@interface UIColor (SensiVita)

+(UIColor *) monTurquoise;
+(UIColor *) monOrange;
+(UIColor *) monVert;
+(UIColor *) monMauve;
+(UIColor *) monRose;
+(UIColor *) monBleu;
+(UIColor *) monOrangeVif;
+(UIColor *) monRouge;

@end

