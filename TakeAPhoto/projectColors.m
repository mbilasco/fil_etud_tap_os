//
//  projectColors.m
//  SensiVita
#import "projectColors.h"

@implementation UIColor (Brague)

+(UIColor *) monTurquoise { return [UIColor colorWithRed:20.0/255.0 green:188.0/255.0 blue:177.0/255.0 alpha:1.0]; }
+(UIColor *) monOrange { return [UIColor colorWithRed:237.0/255.0 green:98.0/255.0 blue:19.0/255.0 alpha:1.0]; }
+(UIColor *) monVert { return [UIColor colorWithRed:112.0/255.0 green:151.0/255.0 blue:51.0/255.0 alpha:1.0]; }
+(UIColor *) monMauve { return [UIColor colorWithRed:128.0/255.0 green:54.0/255.0 blue:137.0/255.0 alpha:1.0]; }
+(UIColor *) monRose { return [UIColor colorWithRed:255.0/255.0 green:153.0/255.0 blue:204.0/255.0 alpha:1.0]; }
+(UIColor *) rougeVoix { return [UIColor colorWithRed:104.0/255.0 green:49.0/255.0 blue:32.0/255.0 alpha:1.0]; }
+(UIColor *) monBleu { return [UIColor colorWithRed:49.0/255.0 green:160.0/255.0 blue:184.0/255.0 alpha:1.0]; }
+(UIColor *) monRouge { return [UIColor colorWithRed:210.0/255.0 green:6.0/255.0 blue:28.0/255.0 alpha:1.0]; }

+(UIColor *) monOrangeVif { return [UIColor colorWithRed:161.0/255.0 green:39.0/255.0 blue:0.0/255.0 alpha:1.0]; }




@end